import React, { Component } from 'react';
import Bookcontent from './Bookcontent'
import Bookdetail from './Bookdetail'
import $ from 'jquery';
import {  Link, Route, Switch } from 'react-router-dom';

class Bookcontainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showing:"",
    books : null,
    keyword : '',
    detail: null,
    inputValue: null,
    bookid: null, rawbook: null
    }
    this.getbooks = this.getbooks.bind(this)
    this.viewbook = this.viewbook.bind(this)
    this.searchbook = this.searchbook.bind(this)
    this.bookdetail = this.bookdetail.bind(this)
    this.seedetail = this.seedetail.bind(this)
    this.sort =this.sort.bind(this)
  }
  
  compare( a, b ) {
    if ( parseInt(a.Price) < parseInt(b.Price) ){
      return 1;
    }
    if ( parseInt(a.Price) > parseInt(b.Price)){
      return -1;
    }
    return 0;
  }

  sort(){
    this.state.rawbook.sort( this.compare );
    let li = []
    for (var i=0;i<this.state.rawbook.length;i++){
      li.push(this.viewbook(this.state.rawbook[i],i))
    }
    this.setState((prevState, props) => {
      return  {books: li};
    });
  }

  viewbook(data,key){
    return(
      <Bookcontent book= {data} key={key} detail={this.bookdetail}/>
    )
  }

  bookdetail(image, data){
    console.log(data)
    this.setState((prevState, props) => {
      return  {detail: {image: image, data:data}};
    });
    this.props.showdetail()
    
  }

  searchbook(){
    let wordlist = this.props.search.split(' ')
    console.log(wordlist)
    $.ajaxSetup({xhrFields: { withCredentials: true } });
    $.post('http://localhost:3001/search',
      {
        "word": wordlist
      },
      function(data, status){
        console.log(data.msg)
        let li = []
        for (var i=0;i<data.msg.length;i++){
            li.push(this.viewbook(data.msg[i],i))
        }
        this.setState((prevState, props) => {
          return  {books: li, rawbook: data.msg};
          });
        }.bind(this));
  }

  seedetail(){
    let li = <Bookdetail data={this.state.detail.data} image={this.state.detail.image}/>
    this.setState((prevState, props) => {
      return {showing: this.props.type, keyword : this.props.search, books: li, inputValue: null};
    });
  }

  getbooks(){
    if(this.state.showing === this.props.type && this.state.keyword === this.props.search){

    }
    else if(this.props.type === "detail"){
      console.log("generate detail")
      this.seedetail()
    }
    else if(this.props.type === "price"){
      console.log("sort by price")
      this.sort()
      this.setState((prevState, props) => {
        return {showing: this.props.type, keyword : this.props.search};
      });
    }
    else if(this.props.type === "search"){
      this.searchbook()
      this.setState((prevState, props) => {
        return {showing: this.props.type, keyword : this.props.search};
      });
    }
    else{
      $.ajaxSetup({xhrFields: { withCredentials: true } });
      $.post('http://localhost:3001/bookcat',
      {
        "cat": this.props.type
      },
      function(data, status){
        let li = []
        for (var i=0;i<data.msg.length;i++){
            li.push(this.viewbook(data.msg[i],i))
        }
        this.setState((prevState, props) => {
          return {showing: this.props.type, books: li, keyword : this.props.search, rawbook: data.msg };
          });
        }.bind(this));
      }
    return (this.state.books)
  }

  render() {
    let position = ""
    if(this.props.type === "all"){
      position = "All books"
    }
    else if(this.props.type === "price"){
      position = "Sort by Highest Price"
    }
    else if(this.props.type === "search"){
      position = "Search Results"
    }
    else if(this.props.type === "detail"){
      position = "Book Detail"
    }
    else{
      position = this.props.type
    }
    let sort = null;
    if(this.props.type === "detail"){

    }
    else{
      sort = <button onClick = {this.props.price} id="sort"> Sort by Price (Highest) </button>
    }
    return(
      <div id = "Bookcontain">
        <button onClick = {this.props.default}> Home </button>
        {sort}
        <h1>{position}</h1>
        <this.getbooks/>
      </div>
    ); 
  }
}

export default Bookcontainer;