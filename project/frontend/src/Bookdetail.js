import React, { Component } from 'react';
import $ from 'jquery';
import {  Link, Route, Switch } from 'react-router-dom';

class Bookdetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    inputValue: null
    }
    this.updateInputValue = this.updateInputValue.bind(this)
    this.order = this.order.bind(this)
  }



  updateInputValue(evt) {
    this.setState((prevState, props) => {
      return {inputValue: evt.target.value};
    });
  }

  order(){
    let quan
    if(this.state.inputValue === null){
      quan = "1"
    }
    else{
      quan = this.state.inputValue
    }
    console.log(this.props.data.BookId, quan)
    $.ajaxSetup({xhrFields: { withCredentials: true } });
    $.post('http://localhost:3001/order',
      {
        "bookid": this.props.data,
        "num": quan
      },
      function(data, status){
        if(data.msg ==="add"){
          alert("success!")
        }
        else{
          alert("failed!")
        }
      }.bind(this));
  }

  render() {
    
    return(
      <div>
        <p>{this.props.data.BookName}</p>
        {this.props.image}
        <p>Author: {this.props.data.Author}</p>
        <p>Publisher: {this.props.data.Publisher}</p>
        <p>Category: {this.props.data.Category}</p>
        <p>Language: {this.props.data.Lang}</p>
        <p>Description: {this.props.data.Description}</p>
        <p>Price: ${this.props.data.Price}</p>
        <label>Oreder: </label>
        <input type="text" value={this.state.inputValue} defaultValue="1" onChange= {(e)=>this.updateInputValue(e)}></input>
        <button onClick={this.order}>Order</button>
      </div>
    ); 
  }
}

export default Bookdetail;