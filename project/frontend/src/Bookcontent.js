import React, { Component } from 'react';
import $ from 'jquery';
import {  Link, Route, Switch } from 'react-router-dom';

class Bookcontent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bookid : '',
      bookimage: null
    }
    this.getimage = this.getimage.bind(this)
    this.godetail = this.godetail.bind(this)
  }

  godetail(){
    this.props.detail(this.state.bookimage, this.props.book)
  }
  getimage(){
    if(this.state.bookid === this.props.book.BookId){
    }
    else{
      $.ajaxSetup({xhrFields: { withCredentials: true } });
      $.post('http://localhost:3001/getbookimage',
      {
        "book": this.props.book.BookId
      },
      function(data, status){
        let src = "data:image/gif;base64," + data.image
        let image = <img src={src}/>
        this.setState((prevState, props) => {
          return {bookimage : image, bookid : this.props.book.BookId};
        });
        }.bind(this));
    }
    return (<ul>{this.state.books}</ul>)
  }

  render() {
    // this.getbooks();
    this.getimage()
    let arrival = null
    if(this.props.book["New Arrival"] === "Yes"){
      arrival = <p class="arrival">NEW ARRIVAL!</p>
    }
    return(
      <div className="Bookcontent">
        <div>
        <Link className = "title" onClick={this.godetail}>{this.props.book.BookName}</Link>
        <br/>
        {this.state.bookimage}
        {arrival}
        <p>Author: {this.props.book.Author}</p>
        <p>Publisher: {this.props.book.Publisher}</p>
        <p>Price: ${this.props.book.Price}</p>
        <br/>
        </div>
      </div> 
    )}
}

export default Bookcontent;