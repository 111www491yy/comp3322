$(document).ready(function () {
    // Populate the commodity list on initial page load
    populateCommodityList();
});

// Functions =============================================================
// Fill commodity list with actual data
function populateCommodityList() {
    // Empty content string
    var listBook = '<table > <tr><th>BookId</th><th>BookName</th><th>Publisher</th><th>Category</th><th>Lang</th>\
    <th>Author</th><th>Description</th><th>Price</th><th>Published</th><th>New Arrival</th></tr>';

   // jQuery AJAX call for JSON
    $.getJSON('/books', function (data) {
        // Put each item in received JSON collection into a <tr> element 
        $.each(data, function () {
            listBook += '<tr><td>' + this.BookId + '</td><td>'+
              this.BookName +'</td><td>' +
              this.Publisher + '</td><td>'+
              this.Category + '</td><td>'+
              this.Lang + '</td><td>'+
              this.Author + '</td><td>'+
              this.Description + '</td><td>'+
              this.Price + '</td><td>'+
              this.Published + '</td><td>'+
              this['New Arrival'] + '</td></tr>';		
        });
        listBook += '</table>'
        
	// Inject the whole commodity list string into our existing #commodityList element
        $('#bookList').html(listBook);
    });
};