Version Info:

MongoDB version v4.4.5
nodejs version v.12.18.2
jquery version v.3.6.0
react version v17.0.2
react-dom version v17.0.2
react-router-dom version v5.2.0
react-scripts version v4.0.3
express version v4.16.1
express-session version v1.17.1


Starting the app:
*** Please note that react-router-dom is installed and used ***

1.Move to file project

2.Start MongoDB database
	mongod --dbpath "path to project"\data

3.Move to file backend (runs on port 3001)
	npm start

4.Move to file frontend (runs on port 3000)
	npm start

5.Access to the app:
	The app runs on http://localhost:3000/


Files in the folder project:

1. The folder "frontend": The react frontend

2. The folder "backend": The express backend

3. The folder "data: The database

4. The file user.json: Stores the initial users data to be inserted into the data base

5. The file book.json: Stores the initial books data to be inserted into the data base


Work completed:

• Correctness of the login page : yes
• Correctness of the create account page :  yes
• Correct functionality of verifying user’s login into the system : yes
• Correct functionality of users’ account creation: yes 
• Correctness of showing the curennt location from home: no
• Correct functionality of the main page : no
• Correct functionality of the category page : yes
• Correct functionality of the book information page : yes
• Correct functionality of the cart page : yes
• Correct functionality of the checkout page : yes
• Correct functionality of the invoice page : yes
• Correct functionality of logout : yes 



